<?php

/**
 * iBay Crawler
 * 
 * @author Ali Haris
 * @copyright 2017
 */

require 'simple_html_dom.php';

function fetchData($url) {
    $html = str_get_html(file_get_contents($url));

    // Find Description
    $description = "";
    foreach($html->find('div.iw-description-div p') as $element) 
        $description .= $element->plaintext . "\n";

    // Find Attributes
    $attr_keys = [];
    $attr_values = [];

    $i = 0;
    foreach ($html->find('.i-detail-table tr td') as $element)  {
        if ($i > 11) break;

        if ($i % 2 != 0) 
            $attr_values[] = trim($element->plaintext);
        else 
            $attr_keys[] = trim($element->plaintext);

        $i++;
    }

    $attrs = array_combine($attr_keys, $attr_values);

    // Find Listing ID
    $meta_data = trim($html->find('.i-item-id', 0)->plaintext);
    $meta_data = explode('|', $meta_data);

    // Extract Listing ID
    preg_match("|\d+|", trim($meta_data[0]), $listing_id);
    $listing_id = $listing_id[0];

    $images = [];
    try {
        $html = str_get_html(file_get_contents('http://ibay.com.mv/index.php?page=images&id='.$listing_id.'&f=f'));

        foreach ($html->find('a') as $element)  {
            preg_match('/"(.*?)"/', $element->href, $matches);
            $images[] = "https:" . $matches[1];
        }
    } catch (\Throwable $t) {
        // Image doesn't exist, skip the error
    }


    return [
        'description' => $description,
        'attributes'  => $attrs,
        'images' => $images
    ];
}

// Real-estate Feed
$source = 'http://ibay.com.mv/external.php?cid=19&lang=en';

$xml = simplexml_load_string(file_get_contents($source));

foreach ($xml->channel->item as $item) {
    $link = $item->link;
    $price = $item->price;

    print_r(fetchData($link));
}

